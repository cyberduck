package ch.cyberduck.core;

/**
 * @version $Id:$
 */
public interface UseragentProvider {

    /**
     * @return User agent string
     */
    String get();
}
