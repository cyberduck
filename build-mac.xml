<?xml version="1.0" encoding="UTF-8"?>
<!--
 *	$Revision: 6757 $
 *	$Date: 2010-08-25 13:25:44 +0200 (Mi, 25 Aug 2010) $
 *
 *  Copyright (c) 2005-2012 David Kocher. All rights reserved.
 *  http://cyberduck.io/
 *
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.package
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	Bug fixes, suggestions and comments should be sent to:
 *	dkocher@cyberduck.io
 -->
<project name="Cyberduck for Mac" basedir=".">

    <import file="build.xml"/>

    <property name="app.bundle" value="${build}/${app.name}.app"/>
    <property name="cli.bundle" value="${build}/duck.bundle"/>

    <property name="bundle.contents" value="Contents"/>
    <property name="bundle.home" value="${bundle.contents}/Home"/>
    <property name="bundle.macos" value="${bundle.contents}/MacOS"/>
    <property name="bundle.library" value="${bundle.contents}/Library"/>
    <property name="bundle.login" value="${bundle.library}/LoginItems"/>
    <property name="bundle.frameworks" value="${bundle.contents}/Frameworks"/>
    <property name="bundle.resources" value="${bundle.contents}/Resources"/>
    <property name="bundle.spotlight" value="${bundle.library}/Spotlight"/>
    <property name="bundle.profiles" value="${bundle.resources}/Profiles"/>
    <property name="bundle.bookmarks" value="${bundle.resources}/Bookmarks"/>
    <property name="bundle.runtime" value="${bundle.macos}/Runtime.jre"/>
    <property name="bundle.runtime.lib"
              value="@executable_path/../MacOS/Runtime.jre/Contents/Home/lib/server/libjvm.dylib"/>

    <property name="app.runtime.system.min" value="10.7"/>

    <property name="jvm.runtime.home" value="${lib}/1.8.0_66.jre"/>
    <property name="jvm.runtime.bin" value="${jvm.runtime.home}/${bundle.home}/bin"/>
    <property name="jvm.runtime.bin" value="${java.home}/bin"/>

    <property name="codesign.certificate" value="Developer ID Application: David Kocher"/>
    <property name="codesign.keychain" value="${user.home}/Library/Keychains/codesign.keychain"/>
    <property name="app.codesign.entitlements" value="${setup}/app/sandbox.entitlements"/>
    <!-- Designated requirement -->
    <property name="codesign.requirement.source" value="${setup}/app/codesign-requirement.txt"/>
    <property name="codesign.requirement" value="${setup}/app/codesign-requirement.bin"/>
    <property name="codesign.options" value="--force"/>
    <property name="codesign.arg"
              value="--entitlements ${app.codesign.entitlements} --requirements ${codesign.requirement}"/>

    <property name="keychain.password" value=""/>

    <property name="installer.certificate" value="3rd Party Mac Developer Installer: David Kocher (G69SCX94XU)"/>
    <property name="installer.keychain" value="${user.home}/Library/Keychains/codesign.keychain"/>

    <property name="sparkle.feed" value="/dev/null"/>

    <property name="spotlight" value="${home}/Spotlight Importer"/>
    <property name="build.spotlight" value="${spotlight}/build/${configuration}"/>

    <property name="build.xcodeoptions" value="SDKROOT=macosx MACOSX_DEPLOYMENT_TARGET=${app.runtime.system.min}"/>

    <property name="build.lipo.binaries" value="**/Contents/MacOS/*,**/*.framework/Versions/Current/*,**/*.dylib"/>
    <property name="build.lipo.arch.remove" value=""/>

    <target name="spotlight">
        <local name="build.settings"/>
        <property name="build.settings"
                  value="${build.xcodeoptions} VERSION=${version} REVISION=${revision} COPYRIGHT='${copyright}'"/>
        <echo message="Build settings ${build.settings}"/>
        <exec dir="${spotlight}" executable="/usr/bin/xcrun" spawn="false" failonerror="true">
            <arg line="--verbose xcodebuild -project 'Spotlight Importer.xcodeproj' -configuration Release ${build.settings}"/>
        </exec>
        <copy todir="${app.bundle}/${bundle.spotlight}">
            <fileset dir="${build.spotlight}">
                <include name="*.mdimporter/**"/>
            </fileset>
        </copy>
        <chmod perm="a+x" type="file">
            <fileset dir="${app.bundle}/${bundle.spotlight}">
                <include name="**/MacOS/*"/>
            </fileset>
        </chmod>
    </target>

    <target name="_dependencies">
        <mkdir dir="${bundle}/${bundle.frameworks}"/>
        <copy todir="${bundle}/${bundle.frameworks}">
            <!-- Dynamic libraries built with xcodebuild -->
            <fileset dir="${build}">
                <include name="*.dylib"/>
            </fileset>
            <fileset dir="${lib}">
                <!-- Include libjnidispatch.dylib -->
                <include name="*.dylib"/>
            </fileset>
        </copy>
        <mkdir dir="${bundle}/${bundle.resources}"/>
        <copy todir="${bundle}/${bundle.resources}">
            <fileset dir="${build}">
                <include name="*.jar"/>
            </fileset>
            <fileset dir="${lib}">
                <include name="*.jar"/>
            </fileset>
        </copy>
        <mkdir dir="${bundle}/${bundle.runtime}/${bundle.home}"/>
        <!-- Copy runtime -->
        <echo message="Copy runtime from ${jvm.runtime.home} to ${bundle}/${bundle.runtime}..."/>
        <copy todir="${bundle}/${bundle.runtime}" preservelastmodified="true">
            <fileset followsymlinks="false" dir="${jvm.runtime.home}" excludesfile="runtime-excludes.properties"/>
        </copy>
        <antcall target="shared-library-install-name">
            <param name="oldname" value="/System/Library/Frameworks/JavaVM.framework/Versions/A/JavaVM"/>
            <param name="newname" value="${bundle.runtime.lib}"/>
            <param name="directory" value="${bundle}/${bundle.frameworks}"/>
        </antcall>
        <!-- Remove architectures -->
        <foreach target="lipo" param="arch" list="${build.lipo.arch.remove}">
            <param name="directory" value="${bundle}"/>
        </foreach>
    </target>

    <target name="_build" depends="spotlight">
        <echo message="Building ${app.bundle}"/>
        <local name="build.settings"/>
        <property name="build.settings"
                  value="${build.xcodeoptions} VERSION=${version} REVISION=${revision} SPARKLEFEED='https://version.cyberduck.io/${app.update.feed}/changelog.rss' COPYRIGHT='${copyright}'"/>
        <echo message="Build settings ${build.settings} for feed ${app.update.feed}"/>
        <exec dir="${home}" executable="/usr/bin/xcrun" spawn="false" failonerror="true">
            <arg line="--verbose xcodebuild -project '${app.name}.xcodeproj' -configuration Release -target app ${build.settings}"/>
        </exec>
        <antcall target="_dependencies">
            <param name="bundle" value="${app.bundle}"/>
        </antcall>
    </target>

    <target name="_cli">
        <echo message="Building ${cli.bundle}"/>
        <property name="build.settings"
                  value="MACOSX_DEPLOYMENT_TARGET=${app.runtime.system.min} VERSION=${version} REVISION=${revision} COPYRIGHT='${copyright}'"/>
        <exec dir="${home}" executable="/usr/bin/xcrun" spawn="false" failonerror="true">
            <arg line="--verbose xcodebuild -project '${app.name}.xcodeproj' -configuration Release -target cli ${build.settings}"/>
        </exec>
        <copy todir="${cli.bundle}/${bundle.frameworks}">
            <!-- Dynamic libraries built with xcodebuild -->
            <fileset dir="${build}">
                <include name="*.dylib"/>
            </fileset>
            <fileset followsymlinks="false" dir="${lib}">
                <!-- Include libjnidispatch.dylib -->
                <include name="*.dylib"/>
            </fileset>
        </copy>
        <copy todir="${cli.bundle}/${bundle.resources}">
            <fileset dir="${build}">
                <include name="config-${version}.jar"/>
                <include name="core-${version}.jar"/>
                <include name="fs-${version}.jar"/>
                <include name="binding-${version}.jar"/>
                <include name="cli-${version}.jar"/>
            </fileset>
            <fileset dir="${lib}">
                <include name="*.jar"/>
            </fileset>
        </copy>
        <mkdir dir="${app.bundle}/${bundle.runtime}/${bundle.home}"/>
        <!-- Copy runtime -->
        <echo message="Copy runtime from ${jvm.runtime.home} to ${cli.bundle}/${bundle.runtime}..."/>
        <copy todir="${cli.bundle}/${bundle.runtime}" preservelastmodified="true">
            <fileset followsymlinks="false" dir="${jvm.runtime.home}" excludesfile="runtime-excludes.properties"/>
        </copy>
        <symlink link="${cli.bundle}/${bundle.runtime}/Contents/MacOS/libjli.dylib"
                 resource="../Home/lib/jli/libjli.dylib"
                 overwrite="true"/>
        <antcall target="shared-library-install-name">
            <param name="oldname" value="/System/Library/Frameworks/JavaVM.framework/Versions/A/JavaVM"/>
            <param name="newname" value="${bundle.runtime.lib}"/>
            <param name="directory" value="${cli.bundle}/${bundle.frameworks}"/>
        </antcall>
        <!-- Remove architectures -->
        <foreach target="lipo" param="arch" list="${build.lipo.arch.remove}">
            <param name="directory" value="${cli.bundle}"/>
        </foreach>
    </target>

    <target name="tarball" depends="revision">
        <antcall target="tar">
            <param name="directory" value="${home}"/>
            <param name="source" value="."/>
            <param name="target" value="duck-src-${version}.${revision}.tar.gz"/>
        </antcall>
        <checksum file="${build.deploy}/duck-src-${version}.${revision}.tar.gz"
                  algorithm="sha-256" property="source.sha256"/>
    </target>

    <target name="package.cli" depends="tarball,cli">
        <antcall target="codesign">
            <param name="bundle" value="${cli.bundle}"/>
            <param name="codesign.options" value="--force"/>
            <!-- No sandboxing enabled -->
            <param name="codesign.arg" value=""/>
            <param name="codesign.certificate" value="Developer ID Application: David Kocher"/>
        </antcall>
        <antcall target="verify-spctl">
            <param name="bundle" value="${cli.bundle}"/>
        </antcall>
        <antcall target="tar">
            <param name="directory" value="${build}"/>
            <param name="source" value="duck.bundle"/>
            <param name="target" value="duck-${version}.${revision}.tar.gz"/>
        </antcall>
        <checksum file="${build.deploy}/duck-${version}.${revision}.tar.gz"
                  algorithm="sha1" property="archive.sha1"/>
        <copy todir="${build.deploy}" overwrite="true">
            <fileset dir="${setup}/brew">
                <include name="*.rb"/>
            </fileset>
        </copy>
        <replace dir="${build.deploy}" summary="true" encoding="UTF-8">
            <include name="*.rb"/>
            <replacefilter token="${REVISION}" value="${revision}"/>
            <replacefilter token="${VERSION}" value="${version}"/>
            <replacefilter token="${ARCHIVE.SHA1}" value="${archive.sha1}"/>
            <replacefilter token="${ARCHIVE}"
                           value="https://dist.duck.sh/duck-${version}.${revision}.tar.gz"/>
            <replacefilter token="${SOURCE.SHA256}" value="${source.sha256}"/>
            <replacefilter token="${SOURCE}"
                           value="https://dist.duck.sh/duck-src-${version}.${revision}.tar.gz"/>
        </replace>
        <exec dir="${build.deploy}" executable="/usr/bin/pkgbuild" spawn="false" failonerror="true">
            <arg line="--component ${cli.bundle} --install-location /usr/local --version ${version} --scripts ${setup}/pkg --sign '${installer.certificate}' --keychain ${codesign.keychain} ${build.deploy}/duck-${version}.${revision}.pkg"/>
        </exec>
        <checksum file="${build.deploy}/duck-${version}.${revision}.pkg" algorithm="md5"/>
        <exec executable="/usr/sbin/pkgutil" failonerror="true">
            <arg line="--check-signature ${build.deploy}/duck-${version}.${revision}.pkg"/>
        </exec>
    </target>

    <target name="package" depends="build">
        <antcall target="codesign">
            <param name="bundle" value="${app.bundle}"/>
            <!-- No sandboxing enabled -->
            <param name="codesign.arg" value="--requirements ${codesign.requirement}"/>
            <param name="codesign.certificate" value="Developer ID Application: David Kocher"/>
        </antcall>
        <antcall target="verify-spctl">
            <param name="bundle" value="${app.bundle}"/>
        </antcall>
        <antcall target="tar">
            <param name="directory" value="${build}"/>
            <param name="target" value="${app.name}-${revision}.tar.gz"/>
            <param name="source" value="${app.name}.app"/>
        </antcall>
        <antcall target="zip">
            <param name="directory" value="${build}"/>
            <param name="target" value="${app.name}-${version}.zip"/>
            <param name="source" value="${app.name}.app"/>
        </antcall>
        <checksum file="${build.deploy}/${app.name}-${revision}.tar.gz"
                  algorithm="md5" property="md5sum"/>
        <echo message="Calculating DSA signature..."/>
        <exec dir="${home}" executable="sh" outputproperty="signature.dsa" failonerror="true">
            <arg value="-o"/>
            <arg value="pipefail"/>
            <arg value="-c"/>
            <arg value="openssl dgst -sha1 -binary '${build.deploy}/${app.name}-${revision}.tar.gz' | openssl dgst -dss1 -sign ${www.update}/private.pem | openssl enc -base64"/>
        </exec>
        <echo message="DSASIGNATURE=${signature.dsa}"/>
        <echo message="MD5SUM=${md5sum}"/>
        <copy todir="${build.deploy}" overwrite="true">
            <fileset dir="${www.update}">
                <include name="changelog.rss"/>
                <include name="changelog.html"/>
            </fileset>
        </copy>
        <replace dir="${build.deploy}" summary="true" encoding="UTF-8">
            <include name="changelog.rss"/>
            <replacefilter token="${TIMESTAMP}" value="${touch.time}"/>
            <replacefilter token="${ARCHIVE}"
                           value="https://update.cyberduck.io/${app.update.feed}/${app.name}-${revision}.tar.gz"/>
            <replacefilter token="${CHANGELOG}" value="https://version.cyberduck.io/${app.update.feed}/changelog.html"/>
            <replacefilter token="${REVISION}" value="${revision}"/>
            <replacefilter token="${VERSION}" value="${version}"/>
            <replacefilter token="${DSASIGNATURE}" value="${signature.dsa}"/>
            <replacefilter token="${COPYRIGHT}" value="${copyright}"/>
            <replacefilter token="${MACOSX_DEPLOYMENT_TARGET}" value="${app.runtime.system.min}"/>
        </replace>
    </target>

    <target name="package.mas" depends="build">
        <replace dir="${app.bundle}/${bundle.contents}" summary="true" encoding="UTF-8">
            <include name="Info.plist"/>
            <!-- Disable Sparkle Keys -->
            <replacefilter token="SU" value="None"/>
        </replace>
        <delete dir="${app.bundle}/${bundle.frameworks}/Sparkle.framework"/>
        <antcall target="codesign">
            <param name="bundle" value="${app.bundle}"/>
            <param name="codesign.arg" value="--entitlements ${app.codesign.entitlements}"/>
            <param name="codesign.certificate" value="3rd Party Mac Developer Application: David Kocher (G69SCX94XU)"/>
        </antcall>
        <echo message="Building ${build.deploy}/${app.name}-${version}.${revision}.pkg"/>
        <exec dir="${build.deploy}" executable="/usr/bin/productbuild" spawn="false" failonerror="true">
            <arg line="--component ${app.bundle} /Applications --version ${version} --sign '${installer.certificate}' --keychain ${installer.keychain} ${build.deploy}/${app.name}-${version}.${revision}.pkg"/>
        </exec>
        <checksum file="${build.deploy}/${app.name}-${version}.${revision}.pkg" algorithm="md5"/>
    </target>

    <target name="lipo">
        <echo message="Remove ${arch} slice from ${build.lipo.binaries}"/>
        <apply executable="/usr/bin/lipo" failonerror="false" dest="${directory}"
               parallel="false" spawn="false" force="true">
            <!--Remove from executables-->
            <!--Remove from frameworks-->
            <!--Remove from shared libraries-->
            <fileset dir="${directory}" includes="${build.lipo.binaries}"/>
            <arg line="-remove ${arch} -output"/>
            <srcfile/>
            <targetfile/>
            <identitymapper/>
        </apply>
    </target>

    <target name="shared-library-install-name">
        <echo message="Change dynamic shared library install names to ${newname}"/>
        <apply executable="/usr/bin/install_name_tool" failonerror="true"
               type="file"
               parallel="false" spawn="false" force="true">
            <arg line="-change ${oldname} ${newname}"/>
            <fileset dir="${directory}">
                <include name="*.dylib"/>
            </fileset>
            <srcfile/>
        </apply>
    </target>

    <target name="codesign" depends="unlock-keychain, compile-codesign-requirement">
        <echo message="Code signing ${bundle} with certificate ${codesign.certificate} and entitlements ${codesign.arg}..."/>
        <apply executable="/usr/bin/codesign" failonerror="true"
               type="both"
               parallel="false" spawn="false" force="true">
            <arg line="${codesign.options} --sign '${codesign.certificate}' --keychain ${codesign.keychain} -v"/>
            <fileset dir="${bundle}/${bundle.spotlight}" erroronmissingdir="false">
                <include name="*.mdimporter"/>
            </fileset>
            <srcfile/>
        </apply>
        <apply executable="/usr/bin/codesign" failonerror="true"
               type="both"
               parallel="false" spawn="false" force="true">
            <arg line="${codesign.options} --identifier ch.sudo.cyberduck --sign '${codesign.certificate}' --keychain ${codesign.keychain} ${codesign.arg} -v"/>
            <fileset dir="${bundle}/${bundle.frameworks}">
                <include name="*.dylib"/>
                <include name="*.jar"/>
            </fileset>
            <srcfile/>
        </apply>
        <apply executable="/usr/bin/codesign" failonerror="true"
               type="both"
               parallel="false" spawn="false" force="true">
            <arg line="${codesign.options} --identifier net.java.openjdk.jre --sign '${codesign.certificate}' --keychain ${codesign.keychain} -v"/>
            <fileset dir="${bundle}/${bundle.runtime}" erroronmissingdir="false">
                <include name="**/*.dylib"/>
                <include name="**/*.jar"/>
            </fileset>
            <fileset dir="${bundle}/${bundle.macos}">
                <include name="*.jre"/>
            </fileset>
            <srcfile/>
        </apply>
        <apply executable="/usr/bin/codesign" failonerror="true"
               type="both"
               parallel="false" spawn="false" force="true">
            <arg line="${codesign.options} --sign '${codesign.certificate}' --keychain ${codesign.keychain} -v"/>
            <fileset dir="${bundle}/${bundle.frameworks}">
                <include name="*.framework/Versions/A"/>
            </fileset>
            <srcfile/>
        </apply>
        <apply executable="/usr/bin/codesign" failonerror="true"
               type="both"
               parallel="false" spawn="false" force="true">
            <arg line="${codesign.options} --sign '${codesign.certificate}' --keychain ${codesign.keychain} ${codesign.arg} -v"/>
            <file name="${bundle}"/>
            <srcfile/>
        </apply>
        <antcall target="verify-signature">
            <param name="bundle" value="${bundle}"/>
        </antcall>
    </target>

    <target name="unlock-keychain">
        <exec executable="/usr/bin/security" failonerror="true">
            <arg line="unlock-keychain -p ${keychain.password} ${codesign.keychain}"/>
        </exec>
    </target>

    <target name="compile-codesign-requirement">
        <echo message="Compile codesign requirement ${codesign.requirement}..."/>
        <exec executable="/usr/bin/csreq" failonerror="true">
            <arg line="-r ${codesign.requirement.source} -b ${codesign.requirement}"/>
        </exec>
    </target>

    <target name="verify-signature">
        <echo message="Print codesign requirement for ${bundle}..."/>
        <exec executable="/usr/bin/codesign" failonerror="true">
            <arg line="-d -r- --deep-verify '${bundle}'"/>
        </exec>
    </target>

    <target name="verify-spctl">
        <echo message="Print system policy verification for ${bundle}..."/>
        <exec executable="/usr/sbin/spctl" failonerror="true">
            <arg line="-v --assess --type execute '${bundle}'"/>
        </exec>
    </target>

    <target name="tar">
        <echo message="Creating tar.gz archive from ${directory}/${source}"/>
        <exec dir="${directory}" executable="tar" spawn="false" failonerror="true">
            <arg line="--exclude .svn --exclude build -czf ${build.deploy}/${target} ${source}"/>
        </exec>
        <echo message="Calculating MD5 sum..."/>
        <checksum file="${build.deploy}/${target}" algorithm="md5"/>
    </target>

    <target name="zip">
        <echo message="Creating ZIP archive from ${directory}/${source}"/>
        <exec dir="${directory}" executable="ditto" spawn="false" failonerror="true">
            <arg line="-c -k --keepParent ${source} ${build.deploy}/${target}"/>
        </exec>
        <checksum file="${build.deploy}/${target}" algorithm="md5"/>
    </target>
</project>
